package businessLogic;

import dbAcces.ClientDB;
import dbAcces.OrderDB;
import model.Order;

import javax.swing.table.DefaultTableModel;

import Interfata.grafica.Gui;
import Interfata.grafica.GuiComanda;

public class OrderBL {
	public static Order selectOrder(int id)
	{
		Order o=OrderDB.select(id);
		return o;
	}
	
	public static int insertOrder(Order o) {
		return OrderDB.insert(o);
	}
	
	public static void deleteOrder(int id)
	{
		OrderDB.delete(id);
	}
	
	public static void updateOrder(int id,int idclient,int idprodus,int cantitate)
	{
	     OrderDB.update(id, idclient,idprodus,cantitate,false);
	}
	
	public static DefaultTableModel makeTable()
	{
		 DefaultTableModel t=OrderDB.tabel();
		 return t;
	}
}
