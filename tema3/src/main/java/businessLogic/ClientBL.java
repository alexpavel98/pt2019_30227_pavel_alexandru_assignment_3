package businessLogic;

import javax.swing.table.DefaultTableModel;

import dbAcces.ClientDB;
import model.*;

public class ClientBL {
	
	public static Client selectClient(int id)
	{
		Client c=ClientDB.select(id);
		return c;
	}
	
	public static int insertClient(Client c) {
		return ClientDB.insert(c);
	}
	
	public static void deleteClient(int id)
	{
		ClientDB.delete(id);
	}
	
	public static void updateClient(int id,String nume,String adresa,String mail,int varsta)
	{
		ClientDB.update(id, nume, adresa, mail, varsta);
	}
	
	public static DefaultTableModel makeTable()
	{
		 DefaultTableModel t=ClientDB.tabel();
		 return t;
	}
}
