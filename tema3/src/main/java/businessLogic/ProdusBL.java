package businessLogic;

import javax.swing.table.DefaultTableModel;

import dbAcces.ClientDB;
import dbAcces.ProdusDB;
import model.Produs;

public class ProdusBL {
	public static Produs selectProdus(int id)
	{
		Produs p=ProdusDB.select(id);
		return p;
	}
	
	public static int insertProdus(Produs p) {
		return ProdusDB.insert(p);
	}
	
	public static void deleteProdus(int id)
	{
		ProdusDB.delete(id);
	}
	
	public static void updateProdus(int id,String denumire,int pret,int cantitate)
	{
		ProdusDB.update(id, denumire,pret,cantitate,true);
	}
	
	public static int updateStoc(int idprodus,int cantitate)
	{
		Produs p=selectProdus(idprodus);
		if(p.getStoc()>=cantitate)
		{
			ProdusDB.update(idprodus, p.getDenumire(), p.getPret(), p.getStoc()-cantitate,false);
			return 0;
		}
		return -1;
	}
	
	public static DefaultTableModel makeTable()
	{
		 DefaultTableModel t=ProdusDB.tabel();
		 return t;
	}
}
