package businessLogic;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

import Interfata.grafica.GuiComanda;
import model.Client;
import model.Order;
import model.Produs;

public class Factura {
	
	private Formatter f;
	public Factura(Order o)
	{
		openFile(o);
		addTxt(o);
		closeFile();
	}
		public void openFile(Order o)
		{
			try
			{
				f=new Formatter("Factura#"+o.getId()+".txt");
			}
			catch(Exception e)
			{
				GuiComanda.creeareFrame("Error", "Eroare la creearea facturii!");
			}
		}
		
		public void addTxt(Order o)
		{
			Client c=ClientBL.selectClient(o.getId_client());
			Produs p=ProdusBL.selectProdus(o.getId_produs());
			DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
			Date date = new Date();
			f.format("\n\n\nFactura #%s \n\n Titular: %s \n\n Emisa la: %s \n\n\n\n\n Produse\n\n %s ......... %s bucati ......... %s lei",o.getId(),c.getNume(),dateFormat.format(date),p.getDenumire(),o.getCantitate(),o.getCantitate()*p.getPret());
		}
		
		public void closeFile()
		{
			f.close();
		}
}
