package dbAcces;

import java.sql.*;
import java.util.logging.*;

import javax.swing.JFrame;
import javax.swing.table.DefaultTableModel;

import Interfata.grafica.GuiProdus;
import conexiune.ConnectionDB;
import model.Produs;

public class ProdusDB {
protected static final Logger LOGGER = Logger.getLogger(ProdusDB.class.getName());
	
    private final static String tabel="SELECT * FROM product";
	private final static String selectProdus="SELECT denumire,pret,stoc FROM product WHERE idproduct=?";
	private final static String countProdus="SELECT COUNT(idproduct) FROM product";
	private static final String insertProdus= "INSERT INTO product (denumire,pret,stoc)"
			+ " VALUES (?,?,?)";
	private final static String deleteProdus="DELETE FROM product WHERE idproduct=?";
	private final static String updateProdus="UPDATE product SET denumire =?,pret = ?,stoc=? WHERE idproduct=?"; 
	
	public static DefaultTableModel tabel()
	{
		Connection dbConnection = ConnectionDB.getConnection();
		PreparedStatement selectAll = null;
		DefaultTableModel t=null;
		ResultSet rs=null;
		try {
			selectAll = dbConnection.prepareStatement(tabel);
			rs=selectAll.executeQuery();
			if (rs.next()) {
				t=RetTabel.creeareTabel(rs);
			}
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "Exceptie la actualizare tabel: " + e.getMessage());
		} finally {
			ConnectionDB.close(rs);
			ConnectionDB.close(selectAll);
			ConnectionDB.close(dbConnection);
		}
		return t;
	}
	
	public static Produs select(int id)
	{
		Connection dbConnection = ConnectionDB.getConnection();
		Produs p=null;
		PreparedStatement selectStatement = null;
		ResultSet rs=null;
		try {
			selectStatement = dbConnection.prepareStatement(selectProdus);
			selectStatement.setInt(1, id);
			rs=selectStatement.executeQuery();
			if (rs.next()) {
				String denumire = rs.getString("denumire");
				int pret = rs.getInt("pret");
				int stoc = rs.getInt("stoc");
				p=new Produs(id, denumire,pret,stoc);
			}
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "Exceptie la cautare produs: " + e.getMessage());
		} finally {
			ConnectionDB.close(rs);
			ConnectionDB.close(selectStatement);
			ConnectionDB.close(dbConnection);
		}
		return p;
	}
	
	public static int count()
	{
		Connection dbConnection = ConnectionDB.getConnection();
		PreparedStatement countStatement = null;
		int c=0;
		ResultSet rs=null;
		try {
			countStatement = dbConnection.prepareStatement(countProdus);
			rs=countStatement.executeQuery();
			if (rs.next()) {
				c = rs.getInt("COUNT(idproduct)");
			}
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "Exceptie la numarare produse: " + e.getMessage());
		} finally {
			ConnectionDB.close(rs);
			ConnectionDB.close(countStatement);
			ConnectionDB.close(dbConnection);
		}
		return c;
	}
	
	public static int insert(Produs p) {
		Connection dbConnection = ConnectionDB.getConnection();

		PreparedStatement insertStatement = null;
		ResultSet rs=null;
		int insertedId = -1;
		try {
			insertStatement = dbConnection.prepareStatement(insertProdus, Statement.RETURN_GENERATED_KEYS);
			insertStatement.setString(1, p.getDenumire());
			insertStatement.setInt(2, p.getPret());
			insertStatement.setInt(3, p.getStoc());
			insertStatement.executeUpdate();

			rs = insertStatement.getGeneratedKeys();
			if (rs.next()) {
				insertedId = rs.getInt(1);
			}
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "Exceptie la introducere produs: " + e.getMessage());
		} finally {
			ConnectionDB.close(rs);
			ConnectionDB.close(insertStatement);
			ConnectionDB.close(dbConnection);
		}
		return insertedId;
	}
	
	public static void delete(int id) {
		Connection dbConnection = ConnectionDB.getConnection();

		PreparedStatement deleteStatement = null;
		try {
			deleteStatement = dbConnection.prepareStatement(deleteProdus, Statement.RETURN_GENERATED_KEYS);
			deleteStatement.setInt(1, id);
			int val=deleteStatement.executeUpdate();
			if(val!=0)
			{
			JFrame inserat=GuiProdus.creeareFrame("Mesaj DB","A fost sters produsul cu id "+id);
			}
			else
			{
			JFrame inserat=GuiProdus.creeareFrame("Mesaj DB","Produsul cu id "+id+" nu exista");
			}
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "Exceptie la stergere produs:  " + e.getMessage());
		} finally {
			ConnectionDB.close(deleteStatement);
			ConnectionDB.close(dbConnection);
		}
	}
	
	public static void update(int id,String denumire,int pret,int stoc,boolean b) 
	{
		Connection dbConnection = ConnectionDB.getConnection();

		PreparedStatement updateStatement = null;
		try {
			updateStatement = dbConnection.prepareStatement(updateProdus, Statement.RETURN_GENERATED_KEYS);
			updateStatement.setString(1, denumire);
			updateStatement.setInt(2, pret);
			updateStatement.setInt(3, stoc);
			updateStatement.setInt(4, id);
			int val=updateStatement.executeUpdate();
			if(val!=0)
			{
				if(b)
				{
			        JFrame inserat=GuiProdus.creeareFrame("Mesaj DB","A fost actualizat produsul cu id "+id);
				}
			}
			else
			{
			JFrame inserat=GuiProdus.creeareFrame("Mesaj DB","Produsul cu id "+id+" nu exista");
			}
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "Exceptie la updatare produs:  " + e.getMessage());
		} finally {
			ConnectionDB.close(updateStatement);
			ConnectionDB.close(dbConnection);
		}
	}
}
