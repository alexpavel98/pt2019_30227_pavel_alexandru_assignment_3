package dbAcces;

import java.sql.*;
import java.util.*;
import javax.swing.table.*;

public class RetTabel {
	
	public static DefaultTableModel creeareTabel(ResultSet rs) throws SQLException 
	{
	    ResultSetMetaData date = rs.getMetaData();
	    Vector<String> numeColoane = new Vector<String>();
	    int nrColoane = date.getColumnCount();
	    for (int i = 1; i <= nrColoane; i++) {
	        numeColoane.add(date.getColumnName(i));
	    }
	    //dupa ce am extras numele coloanelor, populam tabelul cu datele returnate
	    Vector<Vector<Object>> data = new Vector<Vector<Object>>();
	    while (rs.next()) {
	        Vector<Object> vector = new Vector<Object>();
	        for (int i = 1; i <= nrColoane; i++) {
	            vector.add(rs.getObject(i));
	        }
	        data.add(vector);
	    }
	    return new DefaultTableModel(data, numeColoane);
	}
}
