package dbAcces;

import java.sql.*;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.JFrame;
import javax.swing.table.DefaultTableModel;

import Interfata.grafica.GuiComanda;
import conexiune.ConnectionDB;
import model.Order;

public class OrderDB
{
protected static final Logger LOGGER = Logger.getLogger(OrderDB.class.getName());
	
    private final static String tabel="SELECT * FROM orders";
	private final static String selectOrder="SELECT idClient,idproduct,cantitate FROM orders WHERE idOrder=?";
	private final static String countOrder="SELECT COUNT(idOrder) FROM orders";
	private static final String insertOrder= "INSERT INTO orders (idClient,idproduct,cantitate)"
			+ " VALUES (?,?,?)";
	private final static String deleteOrder="DELETE FROM orders WHERE idOrder=?";
	private final static String updateOrder="UPDATE orders SET idClient =?,idproduct = ?,cantitate=? WHERE idOrder=?"; 
	
	public static DefaultTableModel tabel()
	{
		Connection dbConnection = ConnectionDB.getConnection();
		PreparedStatement selectAll = null;
		DefaultTableModel t=null;
		ResultSet rs=null;
		try {
			selectAll = dbConnection.prepareStatement(tabel);
			rs=selectAll.executeQuery();
			if (rs.next()) {
				t=RetTabel.creeareTabel(rs);
			}
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "Exceptie la actualizare tabel: " + e.getMessage());
		} finally {
			ConnectionDB.close(rs);
			ConnectionDB.close(selectAll);
			ConnectionDB.close(dbConnection);
		}
		return t;
	}
	
	public static Order select(int id)
	{
		Connection dbConnection = ConnectionDB.getConnection();
		Order o=null;
		PreparedStatement selectStatement = null;
		ResultSet rs=null;
		try {
			selectStatement = dbConnection.prepareStatement(selectOrder);
			selectStatement.setInt(1, id);
			rs=selectStatement.executeQuery();
			if (rs.next()) {
				int idClient = rs.getInt("idClient");
				int idproduct = rs.getInt("idorder");
				int cantitate = rs.getInt("cantitate");
				o=new Order(id, idClient,idproduct,cantitate);
			}
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "Exceptie la cautare comanda: " + e.getMessage());
		} finally {
			ConnectionDB.close(rs);
			ConnectionDB.close(selectStatement);
			ConnectionDB.close(dbConnection);
		}
		return o;
	}
	
	public static int count()
	{
		Connection dbConnection = ConnectionDB.getConnection();
		PreparedStatement countStatement = null;
		int c=0;
		ResultSet rs=null;
		try {
			countStatement = dbConnection.prepareStatement(countOrder);
			rs=countStatement.executeQuery();
			if (rs.next()) {
				c = rs.getInt("COUNT(idOrder)");
			}
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "Exceptie la numarare comenzi: " + e.getMessage());
		} finally {
			ConnectionDB.close(rs);
			ConnectionDB.close(countStatement);
			ConnectionDB.close(dbConnection);
		}
		return c;
	}
	
	public static int insert(Order o) {
		Connection dbConnection = ConnectionDB.getConnection();

		PreparedStatement insertStatement = null;
		ResultSet rs=null;
		int insertedId = -1;
		try {
			insertStatement = dbConnection.prepareStatement(insertOrder, Statement.RETURN_GENERATED_KEYS);
			insertStatement.setInt(1, o.getId_client());
			insertStatement.setInt(2, o.getId_produs());
			insertStatement.setInt(3, o.getCantitate());
			insertStatement.executeUpdate();

			rs = insertStatement.getGeneratedKeys();
			if (rs.next()) {
				insertedId = rs.getInt(1);
			}
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "Exceptie la creeare comanda: " + e.getMessage());
		} finally {
			ConnectionDB.close(rs);
			ConnectionDB.close(insertStatement);
			ConnectionDB.close(dbConnection);
		}
		return insertedId;
	}
	
	public static void delete(int id) {
		Connection dbConnection = ConnectionDB.getConnection();

		PreparedStatement deleteStatement = null;
		try {
			deleteStatement = dbConnection.prepareStatement(deleteOrder, Statement.RETURN_GENERATED_KEYS);
			deleteStatement.setInt(1, id);
			int val=deleteStatement.executeUpdate();
			if(val!=0)
			{
			JFrame inserat=GuiComanda.creeareFrame("Mesaj DB","A fost sters comanda cu id "+id);
			}
			else
			{
			JFrame inserat=GuiComanda.creeareFrame("Mesaj DB","Comanda cu id "+id+" nu exista");
			}
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "Exceptie la stergere comanda:  " + e.getMessage());
		} finally {
			ConnectionDB.close(deleteStatement);
			ConnectionDB.close(dbConnection);
		}
	}
	
	public static void update(int id,int idclient,int idprodus,int cantitate,boolean flag) 
	{
		Connection dbConnection = ConnectionDB.getConnection();

		PreparedStatement updateStatement = null;
		try {
			updateStatement = dbConnection.prepareStatement(updateOrder, Statement.RETURN_GENERATED_KEYS);
			updateStatement.setInt(1, idclient);
			updateStatement.setInt(2, idprodus);
			updateStatement.setInt(3, cantitate);
			updateStatement.setInt(4, id);
			int val=updateStatement.executeUpdate();
			if(val!=0)
			{
					JFrame inserat=GuiComanda.creeareFrame("Mesaj DB","A fost actualizata comanda cu id "+id);
			}
			else
			{
			JFrame inserat=GuiComanda.creeareFrame("Mesaj DB","Comanda cu id "+id+" nu exista");
			}
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "Exceptie la updatare comanda:  " + e.getMessage());
		} finally {
			ConnectionDB.close(updateStatement);
			ConnectionDB.close(dbConnection);
		}
	}
}