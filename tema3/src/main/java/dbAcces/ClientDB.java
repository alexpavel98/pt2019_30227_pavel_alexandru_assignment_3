package dbAcces;
import java.sql.*;
import java.util.logging.*;

import javax.swing.JFrame;
import javax.swing.table.DefaultTableModel;

import Interfata.grafica.GuiClient;
import conexiune.ConnectionDB;
import model.*;

public class ClientDB {
	
	protected static final Logger LOGGER = Logger.getLogger(ClientDB.class.getName());
	
	private final static String tabel="SELECT * FROM client";
	private final static String selectClient="SELECT nume,adresa,email,varsta FROM client WHERE idClient=?";
	private final static String countClient="SELECT COUNT(idClient) FROM client";
	private static final String insertClient= "INSERT INTO client (nume,adresa,email,varsta)"
			+ " VALUES (?,?,?,?)";
	private final static String deleteClient="DELETE FROM client WHERE idClient=?";
	private final static String updateClient="UPDATE client SET nume =?,adresa = ?,email=?,varsta=? WHERE idClient=?"; 
	
	public static DefaultTableModel tabel()
	{
		Connection dbConnection = ConnectionDB.getConnection();
		PreparedStatement selectAll = null;
		DefaultTableModel t=null;
		ResultSet rs=null;
		try {
			selectAll = dbConnection.prepareStatement(tabel);
			rs=selectAll.executeQuery();
			if (rs.next()) {
				t=RetTabel.creeareTabel(rs);
			}
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "Exceptie la actualizare tabel: " + e.getMessage());
		} finally {
			ConnectionDB.close(rs);
			ConnectionDB.close(selectAll);
			ConnectionDB.close(dbConnection);
		}
		return t;
	}
	
	public static Client select(int id)
	{
		Connection dbConnection = ConnectionDB.getConnection();
		Client c=null;
		PreparedStatement selectStatement = null;
		ResultSet rs=null;
		try {
			selectStatement = dbConnection.prepareStatement(selectClient);
			selectStatement.setInt(1, id);
			rs=selectStatement.executeQuery();
			if (rs.next()) {
				String nume = rs.getString("nume");
				String adresa = rs.getString("adresa");
				String email = rs.getString("email");
				int varsta = rs.getInt("varsta");
				c=new Client(id, nume, adresa, email, varsta);
			}
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "Exceptie la cautare client: " + e.getMessage());
		} finally {
			ConnectionDB.close(rs);
			ConnectionDB.close(selectStatement);
			ConnectionDB.close(dbConnection);
		}
		return c;
	}
	
	public static int count()
	{
		Connection dbConnection = ConnectionDB.getConnection();
		PreparedStatement countStatement = null;
		int c=0;
		ResultSet rs=null;
		try {
			countStatement = dbConnection.prepareStatement(countClient);
			rs=countStatement.executeQuery();
			if (rs.next()) {
				c = rs.getInt("COUNT(idClient)");
			}
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "Exceptie la numarare clienti: " + e.getMessage());
		} finally {
			ConnectionDB.close(rs);
			ConnectionDB.close(countStatement);
			ConnectionDB.close(dbConnection);
		}
		return c;
	}
	
	public static int insert(Client c) {
		Connection dbConnection = ConnectionDB.getConnection();

		PreparedStatement insertStatement = null;
		ResultSet rs=null;
		int insertedId = -1;
		try {
			insertStatement = dbConnection.prepareStatement(insertClient, Statement.RETURN_GENERATED_KEYS);
			insertStatement.setString(1, c.getNume());
			insertStatement.setString(2, c.getAdresa());
			insertStatement.setString(3, c.getEmail());
			insertStatement.setInt(4, c.getVarsta());
			insertStatement.executeUpdate();

			rs = insertStatement.getGeneratedKeys();
			if (rs.next()) {
				insertedId = rs.getInt(1);
			}
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "Exceptie la introducere client: " + e.getMessage());
		} finally {
			ConnectionDB.close(rs);
			ConnectionDB.close(insertStatement);
			ConnectionDB.close(dbConnection);
		}
		return insertedId;
	}
	
	public static void delete(int id) {
		Connection dbConnection = ConnectionDB.getConnection();

		PreparedStatement deleteStatement = null;
		try {
			deleteStatement = dbConnection.prepareStatement(deleteClient, Statement.RETURN_GENERATED_KEYS);
			deleteStatement.setInt(1, id);
			int val=deleteStatement.executeUpdate();
			if(val!=0)
			{
			JFrame inserat=GuiClient.creeareFrame("Mesaj DB","A fost sters clientul cu id "+id);
			}
			else
			{
			JFrame inserat=GuiClient.creeareFrame("Mesaj DB","Clientul cu id "+id+" nu exista");
			}
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "Exceptie la stergere client:  " + e.getMessage());
		} finally {
			ConnectionDB.close(deleteStatement);
			ConnectionDB.close(dbConnection);
		}
	}
	
	public static void update(int id,String nume,String adresa,String email,int varsta) 
	{
		Connection dbConnection = ConnectionDB.getConnection();

		PreparedStatement updateStatement = null;
		try {
			updateStatement = dbConnection.prepareStatement(updateClient, Statement.RETURN_GENERATED_KEYS);
			updateStatement.setString(1, nume);
			updateStatement.setString(2, adresa);
			updateStatement.setString(3, email);
			updateStatement.setInt(4, varsta);
			updateStatement.setInt(5, id);
			int val=updateStatement.executeUpdate();
			if(val!=0)
			{
			JFrame inserat=GuiClient.creeareFrame("Mesaj DB","A fost actualizat clientul cu id "+id);
			}
			else
			{
			JFrame inserat=GuiClient.creeareFrame("Mesaj DB","Clientul cu id "+id+" nu exista");
			}
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "Exceptie la updatare client:  " + e.getMessage());
		} finally {
			ConnectionDB.close(updateStatement);
			ConnectionDB.close(dbConnection);
		}
	}
}
