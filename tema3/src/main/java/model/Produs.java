package model;

public class Produs {
	
	private int id;
	private String denumire;
	private int pret;
	private int stoc;
	
	public Produs(int id,String denumire,int pret,int stoc)
	{
		this.id=id;
		this.denumire=denumire;
		this.pret=pret;
		this.stoc=stoc;
	}
	
	public String getDenumire()
	{
		return denumire;
	}
	
	public int getPret()
	{
		return pret;
	}
	
	public int getStoc()
	{
		return stoc;
	}
}
