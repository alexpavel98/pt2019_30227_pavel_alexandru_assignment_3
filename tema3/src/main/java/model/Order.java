package model;

public class Order {
	private int id;
	private int id_client;
	private int id_produs;
	private int cantitate;
	
	public Order(int id,int id_client,int id_produs,int cantitate)
	{
		this.id=id;
		this.setId_client(id_client);
		this.setId_produs(id_produs);
		this.setCantitate(cantitate);
	}
	
	public void setId(int id)
	{
		this.id=id;
	}

	public int getId()
	{
		return id;
	}
	public int getCantitate() {
		return cantitate;
	}

	public void setCantitate(int cantitate) {
		this.cantitate = cantitate;
	}

	public int getId_produs() {
		return id_produs;
	}

	public void setId_produs(int id_produs) {
		this.id_produs = id_produs;
	}

	public int getId_client() {
		return id_client;
	}

	public void setId_client(int id_client) {
		this.id_client = id_client;
	}
}
