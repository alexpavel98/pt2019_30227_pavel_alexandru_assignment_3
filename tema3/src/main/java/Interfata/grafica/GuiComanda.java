package Interfata.grafica;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.rmi.AccessException;
import java.sql.SQLException;

import javax.swing.*;

import businessLogic.ClientBL;
import businessLogic.Factura;
import businessLogic.OrderBL;
import businessLogic.ProdusBL;
import dbAcces.ClientDB;
import dbAcces.ProdusDB;
import model.Order;

public class GuiComanda extends JFrame{
	public GuiComanda(JPanel p)
	{
		createView();
		p.add(panel);
		p.revalidate();
	}
	
	private JScrollPane tabel;
	private JPanel panel;
	private JLabel title;
	private Button introduOrder,stergeOrder,buton;
	private JTextField id,idclient,idprodus,cantitate;
	
	
	public static JFrame creeareFrame(String titlu,String msg)
	{
		JPanel panel = new JPanel();
		panel.setBackground(new Color(202, 243, 254));
		panel.setPreferredSize(new Dimension(600,200));
		JLabel mesaj=new JLabel(msg,SwingConstants.CENTER);
		mesaj.setBackground(Color.black);
		mesaj.setPreferredSize(new Dimension(600,200));
		mesaj.setForeground(Color.red);
		mesaj.setFont(new Font("Monospaced", Font.BOLD, 16));
		panel.add(mesaj);
		JFrame f=new JFrame();
		f.getContentPane().add(panel);
		f.setTitle(titlu);
		f.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		f.setResizable(true);
		f.pack();
		f.setLocationRelativeTo(null);
		f.setVisible(true);
		return f;
	}
	
	public void removeAL(Button b)
	{
		for(ActionListener al : b.getActionListeners())
		{
			b.removeActionListener(al);
		}
	}
	
	public Button btpreferences(String txt)
	{
		Button b=new Button(txt);
		b.setBackground(new Color(28, 243, 188));
		b.setForeground(Color.white);
		b.setFont(new Font("Monospaced", Font.BOLD, 24));
		b.setPreferredSize(new Dimension(500,50));
		return b;
	}
	
	public JTextField txtpreferences()
	{
		JTextField t=new JTextField();
		t.setBackground(new Color(167, 255, 221));
		t.setPreferredSize(new Dimension(350,40));
		t.setForeground(new Color(167, 255, 221));
		t.setFont(new Font("Monospaced", Font.BOLD, 24));
		return t;
	}
	
	public static JScrollPane retTabel()
	{
		JTable t = new JTable(OrderBL.makeTable());
		t.setBackground(new Color(32,178,170));
		t.setFont(new Font("Serif", Font.BOLD, 16));
		JScrollPane tabel=new JScrollPane(t);
		tabel.setPreferredSize(new Dimension(1080,500));
		tabel.getViewport().setBackground(new Color(167, 255, 221));
		return tabel;
	}
	
	public void createView()
	{
		panel = new JPanel();
		panel.setBackground(new Color(167, 255, 221));
		panel.setPreferredSize(new Dimension(1080,720));
		getContentPane().add(panel);
		
		title = new JLabel("", SwingConstants.CENTER);
		title.setPreferredSize(new Dimension(1920,25));
		title.setForeground(new Color(107, 105, 254));
		title.setFont(new Font("Monospaced", Font.BOLD, 24));
		panel.add(title);
		
		introduOrder=btpreferences("Introducere comanda");
		introduOrder.addActionListener(new IntroduOrder());
		panel.add(introduOrder);
		
		stergeOrder=btpreferences("Sterge comanda");
		stergeOrder.addActionListener(new StergeOrder());
		panel.add(stergeOrder);
		
		id=txtpreferences();
		panel.add(id);
		
		idclient=txtpreferences();
		panel.add(idclient);
		
		idprodus=txtpreferences();
		panel.add(idprodus);
		
		cantitate=txtpreferences();
		panel.add(cantitate);
		
		buton=btpreferences("");
		buton.setPreferredSize(new Dimension(200,40));
		panel.add(buton);
		
		tabel = retTabel();
		panel.add(tabel);
		
	}
	
	private class IntroduOrder implements ActionListener
	{

		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			id.setBackground(new Color(167, 255, 221));
			idclient.setBackground(new Color(152, 114, 255));
			idclient.setText("ID Client");
			idprodus.setBackground(new Color(152, 114, 255));
			idprodus.setText("ID Produs");
			cantitate.setBackground(new Color(152, 114, 255));
			cantitate.setText("Cantitate");
			buton.setLabel("Comanda>>");
			buton.setForeground(Color.red);
			removeAL(buton);
			buton.addActionListener(new Introdu());
		}
		
		private class Introdu extends JFrame implements ActionListener
		{		
			public void actionPerformed(ActionEvent e) {
				try
				{
					if((idclient.getText().equals("ID Client") || idclient.getText().contentEquals("") || idprodus.getText().equals("ID Produs") || idprodus.getText().contentEquals("")||cantitate.getText().equals("Cantitate") ||cantitate.getText().contentEquals("")))
					{
						JFrame exceptie=creeareFrame("ErrorLog","Exceptie: Introduceti toate campurile!");
					}
					else
					{
				    Order o=new Order(0,Integer.parseInt(idclient.getText()),Integer.parseInt(idprodus.getText()),Integer.parseInt(cantitate.getText()));
				    if(ClientDB.select(Integer.parseInt(idclient.getText()))==null || ProdusDB.select(Integer.parseInt(idprodus.getText()))==null)
				    {
				    	throw new SQLException();
				    }
				    if(ProdusBL.updateStoc(Integer.parseInt(idprodus.getText()), Integer.parseInt(cantitate.getText()))==-1)
				    {
				    	 throw new ArithmeticException();
				    }
				    int index=OrderBL.insertOrder(o);
				    JFrame inserat=creeareFrame("Mesaj DB","S-a inregistrat comanda cu id "+index);
				    o.setId(index);
				    Factura f=new Factura(o);
				    panel.remove(tabel);
					tabel = retTabel();
					panel.add(tabel);
					panel.revalidate();
					String wordPadExecutable = "C:\\Program Files (x86)\\Windows NT\\Accessories\\wordpad.exe";
					ProcessBuilder pb=new ProcessBuilder(wordPadExecutable,"Factura#"+index+".txt");
					pb.start();
					}
				   
				}
				catch(NumberFormatException ex)
				{
					JFrame exceptie=creeareFrame("ErrorLog","Exceptie: Eroare de format!");
				}
				catch(SQLException exc)
				{
				     JFrame neinserat=creeareFrame("Mesaj DB","S-a introdus un client/produs invalid! ");
				}
				catch(ArithmeticException stocinsf)
				{
				     JFrame cant=creeareFrame("Mesaj DB","Stoc insuficient! ");
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}

			}
		}
	}
	
	private class StergeOrder implements ActionListener
	{
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			id.setBackground(new Color(152, 114, 255));
			id.setText("ID Comanda");
			idclient.setBackground(new Color(167, 255, 221));
			idprodus.setBackground(new Color(167, 255, 221));
			cantitate.setBackground(new Color(167, 255, 221));
			buton.setLabel("Anuleaza>>");
			buton.setForeground(Color.red);
			removeAL(buton);
			buton.addActionListener(new Sterge());
		}
		
		private class Sterge implements ActionListener
		{
			public void actionPerformed(ActionEvent e) {
				try
				{
					if(id.getText().equals("ID Comanda") || id.getText().contentEquals(""))
					{
						JFrame exceptie=creeareFrame("ErrorLog","Exceptie: Introduceti campul de ID!");
					}
					else
					{
						OrderBL.deleteOrder(Integer.parseInt(id.getText()));
						panel.remove(tabel);
						tabel = retTabel();
						panel.add(tabel);
						panel.revalidate();
					}
				}
				catch(NumberFormatException ex)
				{
					JFrame exceptie=creeareFrame("ErrorLog","Exceptie: Eroare de format!");
				}
			}
		}
	}
}
