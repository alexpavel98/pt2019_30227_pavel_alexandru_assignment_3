package Interfata.grafica;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

import businessLogic.ClientBL;
import businessLogic.ProdusBL;
import model.Produs;

public class GuiProdus extends JFrame{
	public GuiProdus(JPanel p)
	{
		createView();
		p.add(panel);
		p.revalidate();
	}
	
	private JScrollPane tabel;
	private JPanel panel;
	private JLabel title;
	private Button introduProdus,stergeProdus,editeazaProdus,buton;
	private JTextField id,denumire,pret,stoc;
	
	
	public static JFrame creeareFrame(String titlu,String msg)
	{
		JPanel panel = new JPanel();
		panel.setBackground(new Color(202, 243, 254));
		panel.setPreferredSize(new Dimension(600,200));
		JLabel mesaj=new JLabel(msg,SwingConstants.CENTER);
		mesaj.setBackground(Color.black);
		mesaj.setPreferredSize(new Dimension(600,200));
		mesaj.setForeground(Color.red);
		mesaj.setFont(new Font("Monospaced", Font.BOLD, 16));
		panel.add(mesaj);
		JFrame f=new JFrame();
		f.getContentPane().add(panel);
		f.setTitle(titlu);
		f.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		f.setResizable(true);
		f.pack();
		f.setLocationRelativeTo(null);
		f.setVisible(true);
		return f;
	}
	
	public void removeAL(Button b)
	{
		for(ActionListener al : b.getActionListeners())
		{
			b.removeActionListener(al);
		}
	}
	
	public Button btpreferences(String txt)
	{
		Button b=new Button(txt);
		b.setBackground(new Color(28, 243, 188));
		b.setForeground(Color.white);
		b.setFont(new Font("Monospaced", Font.BOLD, 24));
		b.setPreferredSize(new Dimension(300,50));
		return b;
	}
	
	public JTextField txtpreferences()
	{
		JTextField t=new JTextField();
		t.setBackground(new Color(167, 255, 221));
		t.setPreferredSize(new Dimension(350,40));
		t.setForeground(new Color(167, 255, 221));
		t.setFont(new Font("Monospaced", Font.BOLD, 24));
		return t;
	}
	
	public static JScrollPane retTabel()
	{
		JTable t = new JTable(ProdusBL.makeTable());
		t.setBackground(new Color(32,178,170));
		t.setFont(new Font("Serif", Font.BOLD, 16));
		JScrollPane tabel=new JScrollPane(t);
		tabel.setPreferredSize(new Dimension(1080,500));
		tabel.getViewport().setBackground(new Color(167, 255, 221));
		return tabel;
	}
	
	public void createView()
	{
		panel = new JPanel();
		panel.setBackground(new Color(167, 255, 221));
		panel.setPreferredSize(new Dimension(1080,720));
		getContentPane().add(panel);
		
		title = new JLabel("", SwingConstants.CENTER);
		title.setPreferredSize(new Dimension(1920,25));
		title.setForeground(new Color(107, 105, 254));;
		title.setFont(new Font("Monospaced", Font.BOLD, 24));
		panel.add(title);
		
		introduProdus=btpreferences("Introducere produs");
		introduProdus.addActionListener(new IntroduProdus());
		panel.add(introduProdus);
		
		stergeProdus=btpreferences("Sterge produs");
		stergeProdus.addActionListener(new StergeProdus());
		panel.add(stergeProdus);
		
		editeazaProdus=btpreferences("Editare produs");
		editeazaProdus.addActionListener(new EditeazaProdus());
		panel.add(editeazaProdus);
		
		id=txtpreferences();
		panel.add(id);
		
		denumire=txtpreferences();
		panel.add(denumire);
		
		pret=txtpreferences();
		panel.add(pret);
		
		stoc=txtpreferences();
		panel.add(stoc);
		
		buton=btpreferences("");
		buton.setPreferredSize(new Dimension(200,40));
		panel.add(buton);
		
		tabel=retTabel();
		panel.add(tabel);
		
	}
	
	private class IntroduProdus implements ActionListener
	{

		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			id.setBackground(new Color(167, 255, 221));
			denumire.setBackground(new Color(152, 114, 255));
			denumire.setText("Denumire");
			pret.setBackground(new Color(152, 114, 255));
			pret.setText("Pret");
			stoc.setBackground(new Color(152, 114, 255));
			stoc.setText("Stoc");
			buton.setLabel("Introducere>>");
			buton.setForeground(new Color(152, 114, 255));
			removeAL(buton);
			buton.addActionListener(new Introdu());
		}
		
		private class Introdu extends JFrame implements ActionListener
		{		
			public void actionPerformed(ActionEvent e) {
				try
				{
					if((denumire.getText().equals("Denumire") || denumire.getText().contentEquals("") || pret.getText().equals("Pret") || pret.getText().contentEquals("")||stoc.getText().equals("Stoc") ||stoc.getText().contentEquals("")))
					{
						JFrame exceptie=creeareFrame("ErrorLog","Exceptie: Introduceti toate campurile!");
					}
					else
					{
				    Produs p=new Produs(0,denumire.getText(),Integer.parseInt(pret.getText()),Integer.parseInt(stoc.getText()));
				    int index=ProdusBL.insertProdus(p);
					JFrame inserat=creeareFrame("Mesaj DB","S-a introdus un produs nou cu id "+index);
					panel.remove(tabel);
					tabel = retTabel();
					panel.add(tabel);
					panel.revalidate();
					}
				}
				catch(NumberFormatException ex)
				{
					JFrame exceptie=creeareFrame("ErrorLog","Exceptie: Eroare de format!");
				}
			}
		}
	}
	
	private class StergeProdus implements ActionListener
	{
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			id.setBackground(new Color(152, 114, 255));
			id.setText("ID Produs");
			denumire.setBackground(new Color(167, 255, 221));
			pret.setBackground(new Color(167, 255, 221));
			stoc.setBackground(new Color(167, 255, 221));
			buton.setLabel("Stergere>>");
			buton.setForeground(Color.red);
			removeAL(buton);
			buton.addActionListener(new Sterge());
		}
		
		private class Sterge implements ActionListener
		{
			public void actionPerformed(ActionEvent e) {
				try
				{
					if(id.getText().equals("ID Produs") || id.getText().contentEquals(""))
					{
						JFrame exceptie=creeareFrame("ErrorLog","Exceptie: Introduceti campul de ID!");
					}
					else
					{
						ProdusBL.deleteProdus(Integer.parseInt(id.getText()));
						panel.remove(tabel);
						tabel = retTabel();
						panel.add(tabel);
						panel.revalidate();
					}
				}
				catch(NumberFormatException ex)
				{
					JFrame exceptie=creeareFrame("ErrorLog","Exceptie: Eroare de format!");
				}
			}
		}
	}
	
	private class EditeazaProdus implements ActionListener
	{
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			id.setBackground(new Color(152, 114, 255));
			id.setText("ID Produs");
			denumire.setBackground(new Color(152, 114, 255));
			denumire.setText("Denumire");
			pret.setBackground(new Color(152, 114, 255));
			pret.setText("Pret");
			stoc.setBackground(new Color(152, 114, 255));
			stoc.setText("Stoc");
			buton.setLabel("Updateaza>>");
			buton.setForeground(new Color(152, 114, 255));
			removeAL(buton);
			buton.addActionListener(new Editeaza());
		}
		
		private class Editeaza implements ActionListener
		{
			public void actionPerformed(ActionEvent e) {
				try {
					if((id.getText().contentEquals("ID Produs") || id.getText().contentEquals("") || denumire.getText().equals("Denumire") || denumire.getText().contentEquals("") || pret.getText().equals("Pret") || pret.getText().contentEquals("")||stoc.getText().equals("Stoc") ||stoc.getText().contentEquals("")))
					{
						JFrame exceptie=creeareFrame("ErrorLog","Exceptie: Introduceti toate campurile!");
					}
					else
					{
						ProdusBL.updateProdus(Integer.parseInt(id.getText()), denumire.getText(), Integer.parseInt(pret.getText()),Integer.parseInt(stoc.getText()));
						panel.remove(tabel);
						tabel = retTabel();
						panel.add(tabel);
						panel.revalidate();
					}
					
				}
				catch(NumberFormatException ex)
				{
					JFrame exceptie=creeareFrame("ErrorLog","Exceptie: Eroare de format!");
				}
			}
		}
	}
}
