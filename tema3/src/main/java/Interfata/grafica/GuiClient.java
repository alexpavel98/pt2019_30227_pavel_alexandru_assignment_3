package Interfata.grafica;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;
import javax.swing.table.AbstractTableModel;

import businessLogic.ClientBL;
import dbAcces.ClientDB;
import model.Client;

public class GuiClient extends JFrame{
	
	private JPanel panel;
	private JScrollPane tabel;
	private JLabel title;
	private Button introduClient,stergeClient,editeazaClient,buton;
	private JTextField id,nume,adresa,email,varsta;
	public GuiClient(JPanel p)
	{
		createView();
		p.add(panel);
		p.revalidate();
	}
	
	public static JScrollPane retTabel()
	{
		JTable t = new JTable(ClientBL.makeTable());
		t.setBackground(new Color(32,178,170));
		t.setFont(new Font("Serif", Font.BOLD, 16));
		JScrollPane tabel=new JScrollPane(t);
		tabel.setPreferredSize(new Dimension(1080,500));
		tabel.getViewport().setBackground(new Color(167, 255, 221));
		return tabel;
	}
	
	public static JFrame creeareFrame(String titlu,String msg)
	{
		JPanel panel = new JPanel();
		panel.setBackground(new Color(202, 243, 254));
		panel.setPreferredSize(new Dimension(600,200));
		JLabel mesaj=new JLabel(msg,SwingConstants.CENTER);
		mesaj.setBackground(Color.black);
		mesaj.setPreferredSize(new Dimension(600,200));
		mesaj.setForeground(Color.red);
		mesaj.setFont(new Font("Monospaced", Font.BOLD, 16));
		panel.add(mesaj);
		JFrame f=new JFrame();
		f.getContentPane().add(panel);
		f.setTitle(titlu);
		f.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		f.setResizable(true);
		f.pack();
		f.setLocationRelativeTo(null);
		f.setVisible(true);
		return f;
	}
	
	public void removeAL(Button b)
	{
		for(ActionListener al : b.getActionListeners())
		{
			b.removeActionListener(al);
		}
	}
	
	public Button btpreferences(String txt)
	{
		Button b=new Button(txt);
		b.setBackground(new Color(28, 243, 188));
		b.setForeground(Color.white);
		b.setFont(new Font("Monospaced", Font.BOLD, 24));
		b.setPreferredSize(new Dimension(300,50));
		return b;
	}
	
	public JTextField txtpreferences()
	{
		JTextField t=new JTextField();
		t.setBackground(new Color(167, 255, 221));
		t.setPreferredSize(new Dimension(350,40));
		t.setForeground(new Color(167, 255, 221));
		t.setFont(new Font("Monospaced", Font.BOLD, 24));
		return t;
	}
	
	public void createView()
	{
		panel = new JPanel();
		panel.setBackground(new Color(167, 255, 221));
		panel.setPreferredSize(new Dimension(1080,720));
		getContentPane().add(panel);
		
		title = new JLabel("", SwingConstants.CENTER);
		title.setPreferredSize(new Dimension(1920,25));
		title.setForeground(new Color(107, 105, 254));
		title.setFont(new Font("Monospaced", Font.BOLD, 24));
		panel.add(title);
		
		introduClient=btpreferences("Introducere client");
		introduClient.addActionListener(new IntroduClient());
		panel.add(introduClient);
		
		stergeClient=btpreferences("Sterge client");
		stergeClient.addActionListener(new StergeClient());
		panel.add(stergeClient);
		
		editeazaClient=btpreferences("Editare client");
		editeazaClient.addActionListener(new EditeazaClient());
		panel.add(editeazaClient);
		
		id=txtpreferences();
		panel.add(id);
		
		nume=txtpreferences();
		panel.add(nume);
		
		adresa=txtpreferences();
		panel.add(adresa);
		
		email=txtpreferences();
		panel.add(email);
		
		varsta=txtpreferences();
		panel.add(varsta);
		
		buton=btpreferences("");
		buton.setPreferredSize(new Dimension(200,40));
		panel.add(buton);
		
		tabel=retTabel();
		panel.add(tabel);
	}
	
	private class IntroduClient implements ActionListener
	{

		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			id.setBackground(new Color(167, 255, 221));
			nume.setBackground(new Color(152, 114, 255));
			nume.setText("Nume");
			adresa.setBackground(new Color(152, 114, 255));
			adresa.setText("Adresa");
			email.setBackground(new Color(152, 114, 255));
			email.setText("Email");
			varsta.setBackground(new Color(152, 114, 255));
			varsta.setText("Varsta");
			buton.setLabel("Introducere>>");
			buton.setForeground(Color.red);
			removeAL(buton);
			buton.addActionListener(new Introdu());
		}
		
		private class Introdu extends JFrame implements ActionListener
		{		
			public void actionPerformed(ActionEvent e) {
				try
				{
					if((nume.getText().equals("Nume") || nume.getText().contentEquals("") || adresa.getText().equals("Adresa") || adresa.getText().contentEquals("")||email.getText().equals("Email") ||email.getText().contentEquals("")|| varsta.getText().equals("Varsta")||varsta.getText().equals("")))
					{
						JFrame exceptie=creeareFrame("ErrorLog","Exceptie: Introduceti toate campurile!");
					}
					else
					{
				    Client c=new Client(0,nume.getText(),adresa.getText(),email.getText(),Integer.parseInt(varsta.getText()));
				    int index=ClientBL.insertClient(c);
					JFrame inserat=creeareFrame("Mesaj DB","S-a introdus un client nou cu id "+index);
					panel.remove(tabel);
					tabel = retTabel();
					panel.add(tabel);
					panel.revalidate();
					}
				}
				catch(NumberFormatException ex)
				{
					JFrame exceptie=creeareFrame("ErrorLog","Exceptie: Eroare de format!");
				}
			}
		}
	}
	
	private class StergeClient implements ActionListener
	{
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			id.setBackground(new Color(152, 114, 255));
			id.setText("ID Client");
			nume.setBackground(new Color(167, 255, 221));
			adresa.setBackground(new Color(167, 255, 221));
			email.setBackground(new Color(167, 255, 221));
			varsta.setBackground(new Color(167, 255, 221));
			buton.setLabel("Stergere>>");
			buton.setForeground(Color.red);
			removeAL(buton);
			buton.addActionListener(new Sterge());
		}
		
		private class Sterge implements ActionListener
		{
			public void actionPerformed(ActionEvent e) {
				try
				{
					if(id.getText().equals("ID Client") || id.getText().contentEquals(""))
					{
						JFrame exceptie=creeareFrame("ErrorLog","Exceptie: Introduceti campul de ID!");
					}
					else
					{
						ClientBL.deleteClient(Integer.parseInt(id.getText()));
						panel.remove(tabel);
						tabel = retTabel();
						panel.add(tabel);
						panel.revalidate();
					}
				}
				catch(NumberFormatException ex)
				{
					JFrame exceptie=creeareFrame("ErrorLog","Exceptie: Eroare de format!");
				}
			}
		}
	}
	
	private class EditeazaClient implements ActionListener
	{
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			id.setBackground(new Color(152, 114, 255));
			id.setText("ID Client");
			nume.setBackground(new Color(152, 114, 255));
			nume.setText("Nume");
			adresa.setBackground(new Color(152, 114, 255));
			adresa.setText("Adresa");
			email.setBackground(new Color(152, 114, 255));
			email.setText("Email");
			varsta.setBackground(new Color(152, 114, 255));
			varsta.setText("Varsta");
			buton.setLabel("Updateaza>>");
			buton.setForeground(Color.red);
			removeAL(buton);
			buton.addActionListener(new Editeaza());
		}
		
		private class Editeaza implements ActionListener
		{
			public void actionPerformed(ActionEvent e) {
				try {
					if((id.getText().contentEquals("ID Client") || id.getText().contentEquals("") || nume.getText().equals("Nume") || nume.getText().contentEquals("") || adresa.getText().equals("Adresa") || adresa.getText().contentEquals("")||email.getText().equals("Email") ||email.getText().contentEquals("")|| varsta.getText().equals("Varsta")||varsta.getText().equals("")))
					{
						JFrame exceptie=creeareFrame("ErrorLog","Exceptie: Introduceti toate campurile!");
					}
					else
					{
						ClientBL.updateClient(Integer.parseInt(id.getText()), nume.getText(), adresa.getText(), email.getText(), Integer.parseInt(varsta.getText()));
						panel.remove(tabel);
						tabel = retTabel();
						panel.add(tabel);
						panel.revalidate();
					}
					
				}
				catch(NumberFormatException ex)
				{
					JFrame exceptie=creeareFrame("ErrorLog","Exceptie: Eroare de format!");
				}
			}
		}
	}
}