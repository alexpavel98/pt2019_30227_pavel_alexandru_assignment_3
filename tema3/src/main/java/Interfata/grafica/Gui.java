package Interfata.grafica;


import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import javax.swing.*;

import dbAcces.ClientDB;
import dbAcces.OrderDB;
import dbAcces.ProdusDB;



public class Gui extends JFrame{
	private JPanel panel;
	private JLabel title;
	private Button manageClienti,manageProduse,manageOrders;
	public Gui()
	{
		createView();
		setTitle("Management Depozit");
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setResizable(true);
		pack();
		setLocationRelativeTo(null);
		setVisible(true);
	}
	
	public JLabel preferinte(String txt)
	{
		JLabel produse=new JLabel(txt, SwingConstants.CENTER);
		produse.setPreferredSize(new Dimension(1920,100));
		produse.setForeground(Color.getHSBColor(13, 99, 1));
		produse.setFont(new Font("Monospaced", Font.BOLD, 24));
		return produse;
	}
	
	/**
	 * 
	 */
	public void createView()
	{
        
		panel = new JPanel();
		panel.setBackground(new Color(167, 255, 221));
		panel.setPreferredSize(new Dimension(1920,1080));
		getContentPane().add(panel);
		
		title = new JLabel("Management Depozit", SwingConstants.CENTER);
		title.setPreferredSize(new Dimension(1920,70));
		title.setForeground(new Color(57, 187, 207));
		title.setFont(new Font("Monospaced", Font.BOLD, 24));
		panel.add(title);
		
		manageClienti=new Button("Management Clienti");
		manageClienti.setBackground(new Color(152, 114, 255));
		manageClienti.setPreferredSize(new Dimension(300,50));
		manageClienti.setForeground(new Color(167, 255, 221));
		manageClienti.setFont(new Font("", Font.BOLD, 24));
		manageClienti.addActionListener(new Clienti());
		panel.add(manageClienti);
		
		manageProduse=new Button("Management Produse");
		manageProduse.setBackground(new Color(152, 114, 255));
		manageProduse.setPreferredSize(new Dimension(300,50));
		manageProduse.setForeground(new Color(167, 255, 221));
		manageProduse.setFont(new Font("", Font.BOLD, 24));
		manageProduse.addActionListener(new Produse());
		panel.add(manageProduse);
		
		manageOrders=new Button("Management Comenzi");
		manageOrders.setBackground(new Color(152, 114, 255));
		manageOrders.setPreferredSize(new Dimension(300,50));
		manageOrders.setForeground(new Color(167, 255, 221));
		manageOrders.setFont(new Font("", Font.BOLD, 24));
		manageOrders.addActionListener(new Comenzi());
		panel.add(manageOrders);
	}
	
	

	private class Clienti implements ActionListener
	{

		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			try
			{
				panel.remove(4);
			}
			catch(Exception exc)
			{
				
			}
			GuiClient g=new GuiClient(panel);
		}
		
	}
	
	private class Produse implements ActionListener
	{

		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			try
			{
				panel.remove(4);
			}
			catch(Exception exc)
			{
				
			}
			GuiProdus g=new GuiProdus(panel);
		}
		
	}
	
	private class Comenzi implements ActionListener
	{

		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			try
			{
				panel.remove(4);
			}
			catch(Exception exc)
			{
				
			}
			GuiComanda g=new GuiComanda(panel);
		}
		
	}	
	}