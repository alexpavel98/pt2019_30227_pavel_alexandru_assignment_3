package conexiune;

import java.sql.*;
import java.util.logging.Logger;

public class ConnectionDB {
	
	private static final Logger LOGGER=Logger.getLogger(ConnectionDB.class.getName());
	private static final String DRIVER="com.mysql.cj.jdbc.Driver";
	private static final String DBURL="jdbc:mysql://localhost:3306/warehousedb?useSSL=false";
	private static final String USER="root";
	private static final String PASS="alexpavel229";
	
	private static ConnectionDB instanta=new ConnectionDB();
	
	private ConnectionDB()
	{
		try
		{
			Class.forName(DRIVER);
		}
		catch(ClassNotFoundException e)
		{
			e.printStackTrace();
		}
	}
	
	private Connection createConnection()
	{
		Connection con=null;
		try
		{
			con=DriverManager.getConnection(DBURL,USER,PASS);
			System.out.println("BD accesata: "+DBURL);
		}
		catch(SQLException e)
		{
			System.out.println("Nu se poate stabilii conexiunea la baza de date!");
		}
		return con;
	}
	
	public static Connection getConnection()
	{
		return instanta.createConnection();
	}
	
	public static void close(Connection con)
	{
		if(con != null)
		{
			try
			{
				con.close();
			}
			catch(SQLException e)
			{
				
			}
		}

	}
	
	public static void close(Statement stat)
	{
		if(stat != null)
		{
			try
			{
				stat.close();
			}
			catch(SQLException e)
			{
				
			}
		}
	}
	
	public static void close(ResultSet rs)
	{
		if(rs != null)
		{
			try
			{
				rs.close();
			}
			catch(SQLException e)
			{
				
			}
		}
	}
}
